# @gvrs/gatsby-transformer-image-mask

![npm (scoped)](https://img.shields.io/npm/v/@gvrs/gatsby-transformer-image-mask?style=flat-square)
![NPM](https://img.shields.io/npm/l/@gvrs/gatsby-transformer-image-mask?style=flat-square)
![PRs Welcome](https://img.shields.io/badge/PRs-Welcome-brightgreenn?style=flat-square)

Refine edges of placeholders with transparent parts

## Configuration

```javascript
// In your gatsby-config.js
module.exports = {
  plugins: [
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `@gvrs/gatsby-transformer-image-mask`,
  ],
};
```

## Usage example

### Fetching data

```graphql
imageSharp {
  gatsbyImageData
  imageMask {
    base64
  }
}
```

### Rendering

```tsx
import { GatsbyImage } from "gatsby-plugin-image";
import styled from "@emotion/styled";

const GatsbyImageWithMask = styled(GatsbyImage)`
  & [data-placeholder-image] {
    mask-image: url("${(props) => props.mask}");
    mask-size: "contain";
    mask-position: "center center";
    mask-repeat: "no-repeat";
  }
`;

const Image = ({ gatsbyImageData, imageMask }) => (
  <GatsbyImageWithMask image={gatsbyImageData} mask={imageMask.base64} />
);
```

## Returns

- `base64` (string) — base 64 encoded svg mask

## Additional documentation

- [CHANGELOG](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/-/blob/master/CHANGELOG.md)

## See also

- [@gvrs/gatsby-transformer-blurhash](https://gitlab.com/alex.gavrusev/gatsby-transformer-blurhash/) — generate [BlurHash](https://blurha.sh/) placeholders for images
