# [2.0.0](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/compare/v1.0.2...v2.0.0) (2023-01-16)


### Bug Fixes

* **lint:** do not limit body line max length ([1f2aacf](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/commit/1f2aacf668cb6cd5181aa3453ed8b4ec81b1db44))


### Features

* **ci:** use node:18 ([9aac0fc](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/commit/9aac0fc7a5796e41a4f69251e91bc4667bb3eee7))
* **deps:** require gatsby v5 ([99a1fc3](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/commit/99a1fc3d58f22521905118fb4134443138af0ba6))
* use error cause instead of message override ([b06c763](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/commit/b06c76384644b430d5a44e57b65f93b47446a90a))


### BREAKING CHANGES

* **deps:** support for gatsby v4 dropped

## [1.0.2](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/compare/v1.0.1...v1.0.2) (2022-03-08)


### Bug Fixes

* **package.json:** add description ([dfa4e97](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/commit/dfa4e97d7446b56c9e4478f4c274ae45aa493abd))

## [1.0.1](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/compare/v1.0.0...v1.0.1) (2022-03-08)

# 1.0.0 (2022-03-06)


### Features

* initial commit ([b8d43f9](https://gitlab.com/alex.gavrusev/gatsby-transformer-image-mask/commit/b8d43f90cce201d932123d37dcf79af9c58399ff))

## [1.0.2](https://gitlab.com/name-got-flexed/gatsby-transformer-image-mask/compare/v1.0.1...v1.0.2) (2021-03-12)

### Bug Fixes

- move svgo to deps ([e432bab](https://gitlab.com/name-got-flexed/gatsby-transformer-image-mask/commit/e432babb20f7d24631df8c75390aeb2d40940da9))

# 1.0.0 (2021-03-11)

### Features

- initial commit ([10edeb9](https://gitlab.com/name-got-flexed/gatsby-transformer-image-mask/commit/10edeb972cd3d8279135b52e63c3a6ee84f6651c))
