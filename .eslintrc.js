module.exports = {
  extends: ["@gvrs/eslint-config/base"],
  parserOptions: {
    project: "./tsconfig.json",
  },
  env: {
    node: true,
  },
};
