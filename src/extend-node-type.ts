import type { SetFieldsOnGraphQLNodeTypeArgs, PluginOptions } from "gatsby";
import type { FileSystemNode } from "gatsby-source-filesystem";
import { GraphQLObjectType, GraphQLString } from "gatsby/graphql";
import PQueue from "p-queue";
import { cpuCoreCount } from "gatsby-core-utils";
import createDebug from "debug";

import type { NodeWithParent } from "./types";
import generateImageMask from "./generate-image-mask";

const queue = new PQueue({ concurrency: cpuCoreCount() });

const debug = createDebug("gatsby-transformer-image-mask");

const sharpImageMask = (
  args: SetFieldsOnGraphQLNodeTypeArgs,
  _options: PluginOptions
) => {
  const { cache, getNodeAndSavePathDependency } = args;

  return {
    imageMask: {
      type: new GraphQLObjectType({
        name: "SharpImageMask",
        fields: {
          base64: { type: GraphQLString },
        },
      }),
      async resolve(
        image: NodeWithParent,
        _fieldArgs: unknown,
        context: { path: string }
      ) {
        const file = getNodeAndSavePathDependency(
          image.parent,
          context.path
        ) as FileSystemNode;

        const { name } = file;
        const { contentDigest } = file.internal;

        const cacheKey = `transformer-image-mask-${contentDigest}`;

        debug(`Request image mask generation for ${name} (${cacheKey})`);

        return queue.add(async () => {
          const cachedResult: unknown = await cache.get(cacheKey);
          if (cachedResult) {
            debug(`Using cached data for ${name} (${cacheKey})`);
            return cachedResult;
          }

          try {
            debug(
              `Executing image mask generation request for ${name} (${cacheKey})`
            );

            const result = await generateImageMask(file.absolutePath);
            await cache.set(cacheKey, result);
            return result;
          } catch (err: unknown) {
            throw new Error(
              `Unable to generate image mask for ${name} (${cacheKey})`,
              { cause: err }
            );
          }
        });
      },
    },
  };
};

const extendNodeType = (
  args: SetFieldsOnGraphQLNodeTypeArgs,
  _options: PluginOptions
) => {
  const {
    type: { name },
  } = args;

  if (name === "ImageSharp") {
    return sharpImageMask(args, _options);
  }

  return {};
};

export default extendNodeType;
