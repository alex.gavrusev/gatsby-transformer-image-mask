import extendNodeType from "./extend-node-type";

// eslint-disable-next-line import/prefer-default-export
export const setFieldsOnGraphQLNodeType = extendNodeType;
