import { promisify } from "util";
import sharp from "sharp";
import potrace from "potrace";
import temp from "temp";
import { optimize, OptimizedError, OptimizedSvg } from "svgo";
import svgToMiniDataURI from "mini-svg-data-uri";

const trace = promisify(potrace.trace);

const throwIfError: (
  result: OptimizedSvg | OptimizedError
) => asserts result is OptimizedSvg = (result) => {
  if (result.modernError instanceof Error) {
    throw result.modernError;
  }
};

const generateBlurhash = async (imageAbsolutePath: string) => {
  const tempFile = temp.path({ suffix: ".png" });

  await sharp(imageAbsolutePath)
    .ensureAlpha()
    .extractChannel("alpha")
    .negate()
    .png()
    .toFile(tempFile);

  const svg = (await trace(tempFile)) as string;

  const result = optimize(svg, {
    multipass: true,
    floatPrecision: 0,
    plugins: [
      "preset-default",
      {
        name: "removeViewBox",
        active: false,
      },
      "convertPathData",
      {
        name: "addAttributesToSVGElement",
        params: {
          attributes: [{ preserveAspectRatio: "none" }],
        },
      },
    ],
  });

  throwIfError(result);

  const base64 = svgToMiniDataURI(result.data);

  return {
    base64,
  };
};

export default generateBlurhash;
