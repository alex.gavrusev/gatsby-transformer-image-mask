import type { FileSystemNode } from "gatsby-source-filesystem";

export type NodeWithParent = FileSystemNode & { parent: string };
