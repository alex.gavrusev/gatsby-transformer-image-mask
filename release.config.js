const branch = process.env.CI_COMMIT_BRANCH;

const config = {
  branches: ["master", { name: "next", prerelease: true }],
  plugins: [
    [
      "@semantic-release/commit-analyzer",
      {
        preset: "angular",
        releaseRules: [{ type: "docs", scope: "README", release: "patch" }],
      },
    ],
    [
      "@semantic-release/npm",
      {
        pkgRoot: "dist",
        tarballDir: "artifacts",
      },
    ],
    "@semantic-release/release-notes-generator",
  ],
};

// Publish release and changelog only for release branches
if (
  config.branches.some(
    (it) => it === branch || (it.name === branch && !it.prerelease)
  )
) {
  config.plugins.push(
    [
      "@semantic-release/changelog",
      {
        changelogFile: "CHANGELOG.md",
      },
    ],
    [
      "@semantic-release/gitlab",
      {
        assets: [
          {
            path: "artifacts/*.tgz",
            label: "NPM tarball",
          },
        ],
      },
    ],
    [
      "@semantic-release/git",
      {
        assets: ["CHANGELOG.md", "package.json"],
        message:
          // eslint-disable-next-line no-template-curly-in-string
          "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}",
      },
    ]
  );
}

module.exports = config;
